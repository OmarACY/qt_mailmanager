[{]: <region> (header)
# Receptor de correos de Mailgun Routes
[}]: #

[{]: <region> (body)

### Como empezar...

1. Clonar repositorio

       $ git clone https://bitbucket.org/OmarACY/qt_mailmanager.git
	   
2. Moverse a la carpeta del proyecto.
	
		$ cd qt_mailmanager

3. Instalar las dependencias npm.
	
		$ meteor npm install

4. Iniciar aplicación
	
		$ meteor

[}]: #

[{]: <region> (footer)
[{]: <helper> (nav_step)
### 2017
### Contacto ###

Quad Tree, e@quad-tree.com