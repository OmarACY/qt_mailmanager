//Imports
import { Meteor } from 'meteor/meteor';


//Dependencies
var fs = require('fs'); // FileStream para el manejo de archivos
var url = require('url'); // Para parsear las URLs
var http = require('https'); // Para realizar peticiones https
var exec = require('child_process').exec; // Utilizado para crear directorios
var mime = require('mime-types');


Meteor.startup(() => {
  // code to run on server at startup
});

/**
 * Ruta para redireccionar el contenido de los correos de mailgun
 */
JsonRoutes.add("post", "/mailgun/forward", function (req, res, next) {
    var body = req.body;
    result = "forward";
    
    console.log(result);
    console.log(body);

    JsonRoutes.sendResult(res, {
        data: {
            resultado: result
        }
        });
});

/**
 * Ruta para notificar la llegada de un correo de mailgun 
 */
JsonRoutes.add("post", "/mailgun/storeandnotify", function (req, res, next) {
    var body = req.body;
    var result = 'storeandnotify';

    console.log(result);
    console.log(body);

    var attachments = body.attachments;
    var domain = body.domain;

    //Validar que tenga domino
    if(domain != undefined){
        //Filtrar dominios permitidos CHANGE EMAIL DOMAIN ALLOWED
        if(domain == 'YOUR DOMAIN'){
            //Validar que tenga datos adjuntos
            if(attachments != undefined) {
                result = 'OK';
            }else{
                result = 'Correo no valido: Faltan archivos adjuntos';
            }
        }else{
            result = 'Correo no valido: Dominio no identificado';
        }
    }else{
        result = 'Correo no valido: Falta dominio';
    }
    
    //Dar respuesta a mailgun
    JsonRoutes.sendResult(res, {
      data: {
        response: result
      }
    });

    //Descargar archivos adjuntos
    if(result == 'OK'){
        dowloadAttachments(attachments);
    }else{
        console.log(result);
    }
});

/**
 * Metodo para prepara descarga de archivos adjuntos
 * @param {* Links de los archivos adjuntos } attachments 
 */
var dowloadAttachments = function (attachments){

    var download_folder = Meteor.rootPath + '/files/';

    if(createDownloadFolder(download_folder)){

      var jsonAttacments = JSON.parse(attachments);

      for(var i = 0; i < jsonAttacments.length; i ++){

            var attachment = jsonAttacments[i]; 
            var file_url = attachment['url'];
            var content_type = attachment['content-type'];
            var file_name = attachment['name'];
            
            downloadFileHttpget(file_url, file_name + '.' + mime.extension(content_type),download_folder);
        }
    }
};

/**
 * Metodo para la creacion del folder en donde se descargaran los archivos
 * @param {* Nombre del folder } folder_name 
 * @return {* 0 Fallo al crear directorio, 1  Se creo exitosamente el directorio}
 */
var createDownloadFolder = function(folder_name){

    var mkdir = 'mkdir -p ' + folder_name;

    return exec(mkdir, function(err, stdout, stderr) {
        if (err){
          console.log(err); 
          res = 0;
        } 
        else res = 1;

        return res;
    });
}

/**
 * Metodo para descargar archivo desde una URL utilizando HTTPS.get
 * @param {* Link del archivo a descargar } file_url 
 * @param {* Nombre del archivo a descargar } file_name 
 * @param {* Nombre del directorio donde se descargaran los archivos } download_dir 
 */
var downloadFileHttpget = function(file_url,file_name,download_dir) {

    var options = {
        host: url.parse(file_url).host,
        port: 443,
        method: 'GET',
        path: url.parse(file_url).pathname,
        auth: 'YOUR API KEY'
    };

    var file = fs.createWriteStream(download_dir + file_name);

    http.get(options, function(res) {

        res.on('data', function(data) {
            file.write(data);
        }).on('end', function() {
            file.end();
            console.log(file_name + ' descargado en ' + download_dir);
        });

    });
};